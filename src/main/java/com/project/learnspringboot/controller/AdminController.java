package com.project.learnspringboot.controller;

import com.project.learnspringboot.database.entity.AccountEntity;
import com.project.learnspringboot.database.entity.AccountEntityDto;
import com.project.learnspringboot.database.service.AccountEntityService;
import com.project.learnspringboot.model.request.AccountCreateRequest;
import com.project.learnspringboot.model.request.AccountUpdateRequest;
import com.project.learnspringboot.model.response.ApiStatus;
import com.project.learnspringboot.security.CurrentSpringUser;
import com.project.learnspringboot.security.SpringUser;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api")
public class AdminController {

    private final AccountEntityService accountEntityService;

    public AdminController(AccountEntityService accountEntityService) {
        this.accountEntityService = accountEntityService;
    }

    @GetMapping("/status")
    public ResponseEntity<ApiStatus> getStatus(@CurrentSpringUser SpringUser springUser) {
        ApiStatus status = new ApiStatus();
        status.setCode(200);
        status.setMessage("OK");
        return ResponseEntity.ok(status);
    }

    @GetMapping("/account")
    public ResponseEntity<List<AccountEntity>> findAllAccountEntity() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getForEntity("http://localhost:8080", Object.class);

        List<AccountEntity> list = accountEntityService.findAll();
        return ResponseEntity.ok(list);
    }

    @GetMapping("/account-dto")
    public ResponseEntity<List<AccountEntityDto>> findAllAccountDto() {
        List<AccountEntityDto> list = accountEntityService.findAllDto();
        return ResponseEntity.ok(list);
    }

    @GetMapping("/account/{id}")
    public ResponseEntity<AccountEntity> findAccountEntityById(@PathVariable UUID id) {
        AccountEntity accountEntity = accountEntityService.getById(id);
        return ResponseEntity.ok(accountEntity);
    }

    @PostMapping("/account")
    public ResponseEntity<AccountEntity> createAccountEntity(@RequestBody @Valid AccountCreateRequest request) {
        AccountEntity accountEntity = accountEntityService.create(request);
        return ResponseEntity.ok(accountEntity);
    }

    @PutMapping("/account/{id}")
    public ResponseEntity<AccountEntity> updateAccountEntity(
            @PathVariable UUID id, @RequestBody @Valid AccountUpdateRequest request) {
        AccountEntity accountEntity = accountEntityService.update(id, request);
        return ResponseEntity.ok(accountEntity);
    }

    @DeleteMapping("/account/{id}")
    public ResponseEntity<AccountEntity> deleteAccountEntity(@PathVariable UUID id) {
        AccountEntity accountEntity = accountEntityService.delete(id);
        return ResponseEntity.ok(accountEntity);
    }
}

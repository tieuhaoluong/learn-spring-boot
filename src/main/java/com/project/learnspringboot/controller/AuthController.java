package com.project.learnspringboot.controller;

import com.project.learnspringboot.database.entity.AccountEntity;
import com.project.learnspringboot.database.service.AccountEntityService;
import com.project.learnspringboot.model.request.AccountLoginRequest;
import com.project.learnspringboot.model.request.AccountRegisterRequest;
import com.project.learnspringboot.model.response.AccessToken;
import com.project.learnspringboot.security.JwtTokenProvider;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;
    private final AccountEntityService accountEntityService;

    public AuthController(PasswordEncoder passwordEncoder, JwtTokenProvider jwtTokenProvider, AccountEntityService accountEntityService) {
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenProvider = jwtTokenProvider;
        this.accountEntityService = accountEntityService;
    }

    @PostMapping("/register")
    public ResponseEntity<AccessToken> register(@Valid @RequestBody AccountRegisterRequest form) {
        AccountEntity account = accountEntityService.register(form);
        AccessToken accessToken = jwtTokenProvider.createToken(account);
        return ResponseEntity.ok(accessToken);
    }

    @PostMapping("/login")
    public ResponseEntity<AccessToken> login(@Valid @RequestBody AccountLoginRequest form) {
        AccountEntity account = accountEntityService.findByUsername(form.getUsername())
                .orElseThrow(() -> new RuntimeException("Username not found"));
        if (!passwordEncoder.matches(form.getPassword(), account.getPassword())) {
            throw new RuntimeException("Wrong password");
        }
        AccessToken accessToken = jwtTokenProvider.createToken(account);
        return ResponseEntity.ok(accessToken);
    }
}

package com.project.learnspringboot.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiStatus {
    private int code;
    private String message;
}

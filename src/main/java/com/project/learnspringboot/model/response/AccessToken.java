package com.project.learnspringboot.model.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@Builder
public class AccessToken {
    private String id;
    private String subject;
    private String type;
    private String token;
    private Instant expireAt;
}

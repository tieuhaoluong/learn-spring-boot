package com.project.learnspringboot.model.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountLoginRequest {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
}

package com.project.learnspringboot.model.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountUpdateRequest {
    @NotBlank
    private String fullName;
    @NotNull
    private Gender gender;
    private String phone;
    private String address;
}

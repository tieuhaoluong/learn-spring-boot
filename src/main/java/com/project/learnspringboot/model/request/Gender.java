package com.project.learnspringboot.model.request;

public enum Gender {
    MALE, FEMALE;
}

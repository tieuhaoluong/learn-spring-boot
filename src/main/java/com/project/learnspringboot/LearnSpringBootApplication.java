package com.project.learnspringboot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;

@Slf4j
@SpringBootApplication
public class LearnSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(LearnSpringBootApplication.class);
        ConfigurableApplicationContext applicationContext = application.run(args);
        Environment environment = applicationContext.getEnvironment();
        logApplicationStartup(environment);
    }

    private static void logApplicationStartup(Environment environment) {
        String protocol = "http";
        if (Boolean.parseBoolean(environment.getProperty("server.ssl.enabled")) && environment.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        String appName = environment.getProperty("app.name");
        String appVersion = environment.getProperty("app.version");
        String appUrl = environment.getProperty("app.deploy-url");
        String appStorage = environment.getProperty("app.storage", "");
        String serverPort = environment.getProperty("server.port");
        String contextPath = environment.getProperty("server.servlet.context-path", "");
        String hostAddress = "localhost";
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.warn("The host name could not be determined, using `localhost` as fallback");
        }

        String localUrl = protocol + "://" + hostAddress + ":" + serverPort + contextPath;
        log.info(
                """

                        ----------------------------------------------------------
                        \tApplication: \t{}-v{}
                        \tLocal URL: \t\t{}
                        \tDeploy URL: \t{}
                        \tStorage: \t\t{}
                        \tProfile(s): \t{}
                        ----------------------------------------------------------""",
                appName,
                appVersion,
                localUrl,
                appUrl,
                Path.of(appStorage).toAbsolutePath(),
                environment.getActiveProfiles()
        );
    }

}

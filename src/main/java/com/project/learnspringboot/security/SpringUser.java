package com.project.learnspringboot.security;

import com.project.learnspringboot.database.entity.AccountEntity;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Getter
public class SpringUser implements UserDetails {
    private final AccountEntity accountEntity;
    private final List<GrantedAuthority> grantedAuthorities;
    private final String username;
    private final String password;
    private final boolean enabled;
    private final boolean accountNonLocked;

    public SpringUser(AccountEntity accountEntity) {
        this.accountEntity = accountEntity;
        this.username = accountEntity.getUsername();
        this.password = accountEntity.getPassword();
        this.enabled = true;
        this.accountNonLocked = true;
        this.grantedAuthorities = Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isEnabled();
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isEnabled();
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}

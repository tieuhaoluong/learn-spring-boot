package com.project.learnspringboot.security;

import com.project.learnspringboot.database.entity.AccountEntity;
import com.project.learnspringboot.database.service.AccountEntityService;
import com.project.learnspringboot.model.response.AccessToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;

import javax.crypto.SecretKey;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

@Slf4j
@Configuration
public class JwtTokenProvider {
    public static final String AUTH_TOKEN_TYPE = "Bearer";
    private final AccountEntityService accountEntityService;

    public JwtTokenProvider(AccountEntityService accountEntityService) {
        this.accountEntityService = accountEntityService;
    }

    public AccessToken createToken(AccountEntity account) {
        SecretKey secretKey = Keys.hmacShaKeyFor(getSecretKey());
        String id = UUID.randomUUID().toString();
        String subject = account.getId().toString();
        Instant issuedAt = Instant.now();
        Instant expiration = issuedAt.plus(Duration.ofMinutes(10));
        String token = Jwts.builder()
                .id(id)
                .subject(account.getId().toString())
                .issuedAt(Date.from(issuedAt))
                .expiration(Date.from(expiration))
                .signWith(secretKey)
                .compact();
        return AccessToken.builder()
                .id(id)
                .subject(subject)
                .type(AUTH_TOKEN_TYPE)
                .token(token)
                .expireAt(expiration)
                .build();
    }

    public String resolveToken(HttpServletRequest request) {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (StringUtils.hasText(token) && token.startsWith(AUTH_TOKEN_TYPE)) {
            String jwtToken = token.substring(AUTH_TOKEN_TYPE.length() + 1);
            if (StringUtils.hasText(jwtToken) && !jwtToken.equals("null"))
                return jwtToken;
        }
        return null;
    }

    public Claims validateToken(String token, byte[] secret) {
        try {
            if (StringUtils.hasText(token)) {
                SecretKey secretKey = Keys.hmacShaKeyFor(secret);
                return Jwts.parser()
                        .verifyWith(secretKey).build()
                        .parseSignedClaims(token)
                        .getPayload();
            }
        } catch (Exception e) {
            log.error("ValidateTokenException: {}", e.getMessage());
        }
        return null;
    }

    public void resolveAndValidateToken(HttpServletRequest request) {
        String resolvedToken = resolveToken(request);
        Claims claims = validateToken(resolvedToken, getSecretKey());
        if (claims != null) {
            UUID claimId = parseUUID(claims.getSubject());
            if (claimId != null) {
                accountEntityService.findById(claimId).ifPresent(entity -> {
                    SpringUser springUser = new SpringUser(entity);
                    Authentication authentication = new UsernamePasswordAuthenticationToken(springUser, resolvedToken, springUser.getAuthorities());
                    if (!springUser.isEnabled()) {
                        authentication.setAuthenticated(false);
                    }
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                });
            }
        }
    }

    private byte[] getSecretKey() {
        return "random-text-secret-key-1234567890_1234567890".getBytes();
    }

    private UUID parseUUID(String id) {
        try {
            return UUID.fromString(id);
        } catch (Exception e) {
            return null;
        }
    }
}

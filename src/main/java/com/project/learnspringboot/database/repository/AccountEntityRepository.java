package com.project.learnspringboot.database.repository;

import com.project.learnspringboot.database.entity.AccountEntity;
import com.project.learnspringboot.database.entity.AccountEntityDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AccountEntityRepository extends JpaRepository<AccountEntity, UUID> {
    Optional<AccountEntity> findByUsername(String username);

    boolean existsByUsernameOrEmail(String username, String email);

    boolean existsByUsername(String username);

    @Query("select new com.project.learnspringboot.database.entity.AccountEntityDto(a.id, a.username, a.email, a.fullName, a.gender, a.phone, a.address) from AccountEntity a")
    List<AccountEntityDto> findAllDto();

}

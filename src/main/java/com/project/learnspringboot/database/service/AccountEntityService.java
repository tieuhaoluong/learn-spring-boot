package com.project.learnspringboot.database.service;

import com.project.learnspringboot.database.entity.AccountEntity;
import com.project.learnspringboot.database.entity.AccountEntityDto;
import com.project.learnspringboot.database.repository.AccountEntityRepository;
import com.project.learnspringboot.model.request.AccountCreateRequest;
import com.project.learnspringboot.model.request.AccountRegisterRequest;
import com.project.learnspringboot.model.request.AccountUpdateRequest;
import com.project.learnspringboot.model.request.Gender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AccountEntityService {
    private final PasswordEncoder passwordEncoder;
    private final AccountEntityRepository accountEntityRepository;

    public AccountEntityService(PasswordEncoder passwordEncoder, AccountEntityRepository accountEntityRepository) {
        this.passwordEncoder = passwordEncoder;
        this.accountEntityRepository = accountEntityRepository;
    }

    @Transactional
    public AccountEntity register(AccountRegisterRequest request) {
        boolean exists = accountEntityRepository.existsByUsernameOrEmail(request.getUsername(), request.getEmail());
        if (exists) {
            throw new RuntimeException("Account already exists");
        }
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setUsername(request.getUsername());
        accountEntity.setEmail(request.getEmail());
        accountEntity.setFullName(request.getFullName());
        accountEntity.setPassword(passwordEncoder.encode(request.getPassword()));
        accountEntity.setGender(Gender.MALE);
        accountEntity.setCreatedDate(new Date());
        accountEntityRepository.save(accountEntity);
        return accountEntity;
    }

    @Transactional
    public AccountEntity create(AccountCreateRequest request) {
        if (accountEntityRepository.existsByUsername(request.getUsername())) {
            throw new RuntimeException("username already exists");
        }
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setUsername(request.getUsername());
        accountEntity.setEmail(request.getEmail());
        accountEntity.setPassword(passwordEncoder.encode(request.getPassword()));
        accountEntity.setFullName(request.getFullName());
        accountEntity.setGender(Gender.MALE);
        accountEntity.setCreatedDate(new Date());
        accountEntityRepository.save(accountEntity);
        return accountEntity;
    }

    @Transactional
    public AccountEntity update(UUID id, AccountUpdateRequest request) {
        AccountEntity accountEntity = accountEntityRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        accountEntity.setFullName(request.getFullName());
        accountEntity.setGender(request.getGender());
        accountEntity.setPhone(request.getPhone());
        accountEntity.setAddress(request.getAddress());
        accountEntityRepository.save(accountEntity);
        return accountEntity;
    }

    @Transactional
    public AccountEntity delete(UUID id) {
        AccountEntity accountEntity = accountEntityRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        accountEntityRepository.delete(accountEntity);
        return accountEntity;
    }

    public boolean existsByUsernameOrEmail(String username, String email) {
        return accountEntityRepository.existsByUsernameOrEmail(username, email);
    }

    public Optional<AccountEntity> findByUsername(String username) {
        return accountEntityRepository.findByUsername(username);
    }

    public List<AccountEntity> findAll() {
        return accountEntityRepository.findAll();
    }

    public List<AccountEntityDto> findAllDto() {
        return accountEntityRepository.findAllDto();
    }

    public AccountEntity getById(UUID id) {
        return accountEntityRepository.findById(id).orElseThrow(() -> new RuntimeException("Account not found"));
    }

    public Optional<AccountEntity> findById(UUID id) {
        return accountEntityRepository.findById(id);
    }
}

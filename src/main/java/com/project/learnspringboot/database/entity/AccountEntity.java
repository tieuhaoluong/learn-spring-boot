package com.project.learnspringboot.database.entity;

import com.project.learnspringboot.model.request.Gender;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "tbl_account")
public class AccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public UUID id;

    @Column(name = "username", length = 50, unique = true, nullable = false)
    public String username;

    @Column(name = "email", length = 200, unique = true, nullable = false)
    public String email;

    @Column(name = "password", length = 100, nullable = false)
    private String password;

    @Column(name = "full_name", length = 100, nullable = false)
    public String fullName;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "gender", nullable = false)
    public Gender gender;

    @Column(name = "phone", length = 20)
    public String phone;

    @Column(name = "address", length = 200)
    public String address;

    @Column(name = "created_date", columnDefinition = "timestamp with time zone")
    public Date createdDate;
}

package com.project.learnspringboot.database.entity;

import com.project.learnspringboot.model.request.Gender;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/**
 * DTO for {@link AccountEntity}
 */
@AllArgsConstructor
@Getter
public class AccountEntityDto implements Serializable {
    private final UUID id;
    private final String username;
    private final String email;
    private final String fullName;
    private final Gender gender;
    private final String phone;
    private final String address;
}

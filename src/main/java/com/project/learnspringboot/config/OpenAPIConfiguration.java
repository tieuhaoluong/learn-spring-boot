package com.project.learnspringboot.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.SpecVersion;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;

@Configuration
public class OpenAPIConfiguration {
    public static final String SECURITY_SCHEMA_NAME = "JWT";

    @Bean
    public OpenAPI customOpenAPI() {
        Info info = new Info()
                .title("Learn Spring Boot")
                .version("1.0");

        SecurityScheme securityScheme = new SecurityScheme()
                .type(SecurityScheme.Type.HTTP)
                .name(HttpHeaders.AUTHORIZATION)
                .in(SecurityScheme.In.HEADER)
                .scheme("bearer")
                .bearerFormat("JWT");

        Components components = new Components();
        components.addSecuritySchemes(SECURITY_SCHEMA_NAME, securityScheme);

        return new OpenAPI(SpecVersion.V31)
                .info(info)
                .components(components)
                .addSecurityItem(new SecurityRequirement().addList(SECURITY_SCHEMA_NAME));
    }

    @Bean
    public GroupedOpenApi groupedOpenApi() {
        return GroupedOpenApi.builder()
                .group("learn-api")
                .displayName("Learn-Api")
                .pathsToMatch("/api/**", "/actuator/**")
                .build();
    }
}
